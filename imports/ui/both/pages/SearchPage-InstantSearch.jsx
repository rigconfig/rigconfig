// NOT USED
import { Meteor } from 'meteor/meteor';
import React from "react";
// import ReactDOM from 'react-dom';
import Helmet from 'react-helmet';
import AppConfig from '/imports/startup/both/AppConfig.js';
import { createContainer } from 'meteor/react-meteor-data';
// import AppState from '/imports/startup/both/AppState.js';
import {InstantSearch, Hits, SearchBox, hightlight, RefinementList, Pagination, CurrentRefinements, ClearAll} from 'react-instantsearch/dom';

// if (Meteor.isClient) {
//   import {InstantSearch, Hits, SearchBox, hightlight, RefinementList, Pagination, CurrentRefinements, ClearAll} from 'react-instantsearch/dom';
// }

function Product({hit}) {
  return (
    <div>
      <span className="hit-name">{hit.name}</span>
    </div>
  )
}

export default class SearchPage extends React.Component {
  // componentDidMount() {
    // console.log('Mounted!');
  // }
  render() {
    return (
      <section className="page">
        <div>
          <Helmet
            title={'Search Page' + ' @ ' + AppConfig.site.title}
          />
          <h2>Search</h2>
          <InstantSearch
            appId={"latency"}
            apiKey={"3d9875e51fbd20c7754e65422f7ce5e1"}
            indexName={"bestbuy"}
          >
            <div className='container'>
              <CurrentRefinements/>
              <ClearAll/>
              <SearchBox />
              <RefinementList attributeName="category" />
              <Hits itemComponent={Product} />
              <Pagination />
            </div>
          </InstantSearch>

        </div>
      </section>
    )
  }
}

/*
const SearchPageServer = () => {
  return (
    <p>server</p>
  )
}

let page = Meteor.isServer ? SearchPageServer : SearchPage

export default createContainer(({params}) => {
  return {}
}, page)
*/
