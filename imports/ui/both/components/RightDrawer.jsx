import { Meteor } from 'meteor/meteor';
import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import AppState from '/imports/startup/both/AppState.js';
import { Scrollbars } from 'react-custom-scrollbars';
import RigConfigurator from '/imports/ui/both/components/RigConfigurator/RigConfigurator.jsx';

export default function RightDrawer({initialRender, drawerStates}) {
  return (
    <div id="RightDrawer" className={drawerStates}>
      <Scrollbars
        autoHide
        autoHideTimeout={500}
        autoHideDuration={500}
        universal={true}
        thumbMinSize={0}
      >
        <div className="RightDrawer-container">
          { !initialRender && <RigConfigurator /> }
        </div>
      </Scrollbars>
    </div>
  )
}
