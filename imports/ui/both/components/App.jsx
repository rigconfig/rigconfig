import { Meteor } from 'meteor/meteor';
import React from "react";
import { Button } from "react-bootstrap";
import Helmet from 'react-helmet';
import { createContainer } from 'meteor/react-meteor-data';
import classnames from 'classnames';
import AppConfig from '/imports/startup/both/AppConfig.js';
import AppState from '/imports/startup/both/AppState.js';
import Actions from '/imports/startup/both/Actions.js';
import Navbar from "/imports/ui/both/components/Navbar.jsx";
import RightDrawer from "/imports/ui/both/components/RightDrawer.jsx";
import LeftDrawer from "/imports/ui/both/components/LeftDrawer.jsx";
import UserSettingsModal from "/imports/ui/both/modals/UserSettingsModal.jsx";
import { algoliaInclude } from '/imports/api/lib/algolia-include.js';
import { heapInclude } from '/imports/api/lib/heap-include.js';

class App extends React.Component {
  componentDidMount() {
    (this.props.initialRender === true) && AppState.set({initialRender: false})
  }

  render() {
    const { defaultTitle, pageTitle, canonicalURL, initialRender, UserSettingsModalOpen, drawerStates, children } = this.props

    return (
      <div id="App" className={drawerStates}>
        <Helmet
          {...defaultTitle}
          title={pageTitle}
          link={[
            {"rel": "stylesheet", "href": "https://cdn.jsdelivr.net/fontawesome/4.7.0/css/font-awesome.min.css"},
            {"rel": "canonical", "href": canonicalURL},
            {"rel": "apple-touch-icon", "sizes": "180x180", "href": "/apple-touch-icon.png"},
            {"rel": "icon", "type":"image/png", "href": "/favicon-32x32.png", "sizes": "32x32"},
            {"rel": "icon", "type":"image/png", "href": "/favicon-16x16.png", "sizes": "16x16"},
            {"rel": "manifest", "href": "/manifest.json"},
            {"rel": "mask-icon", "href": "/safari-pinned-tab.svg", "color": "#ee0b0b"}
          ]}
          meta={[
            {"name": "apple-mobile-web-app-title", "content": "RigConfig"},
            {"name": "application-name", "content": "RigConfig"},
            {"name": "theme-color", "content": "#ffffff"}
          ]}
        />

        { children }

        <Navbar />

        <LeftDrawer { ...{initialRender, drawerStates } } />

        <RightDrawer { ...{initialRender, drawerStates } } />

        <Helmet
          script={[{
            "type": "text/javascript",
            // "src": "//cdn.jsdelivr.net/g/algoliasearch@3.18.0(algoliasearchLite.js),algoliasearch.helper@2.12.0(algoliasearch.helper.js),autocomplete.js@0.21.3(autocomplete.min.js)"
            "src": "/a/j/algoliasearch_3.18.0.js"
          },
          {
            "type": "text/javascript",
            // "innerHTML": algoliaInclude
            "innerHTML": algoliaInclude
          },
          {
            "type": "text/javascript",
            "innerHTML": heapInclude
          },
          {
            "type": "text/javascript",
            "src": "/a/j/Sortable.js"
          },
          {
            "type": "text/javascript",
            "innerHTML": `
              function initSortable() {
                var el = document.getElementById('components-list');
                // console.log(el)
                var sortable = new Sortable.create(el, {
                  draggable: ".rig-component",
                  onEnd(e) {
                    Actions.recalculateSortOrder();
                  }
                });
              }
              setTimeout(initSortable, 3000);
              // @todo - race condition?
            `
          }
        ]}
        />

        <div className="site-footer">
          <p className="" style={{float: 'left'}}>
            {/* <a href="https://gitlab.com/rigconfig/ccdb" target="ccdb">Add Components</a> */}
            <Button
              bsSize="small"
              className="add-components-link"
              bsStyle="link"
              href="https://gitlab.com/rigconfig/ccdb#ccdb-computer-component-database"
              target="ccdb"
            >
              <i className="fa fa-plus"></i>
              <span>&nbsp;Add Components</span>
            </Button>
          </p>
          <p style={{float: 'right'}}>
            <a href="/privacy-policy.html" rel="nofollow" target="_privacy-policy">Privacy Policy</a>
            <span className="divider">|</span>
            <a href="/tos.html" rel="nofollow" target="_tos">TOS</a>
          </p>
        </div>

        <UserSettingsModal UserSettingsModalOpen={UserSettingsModalOpen} />
      </div>
    )
  }
}

export default createContainer(({location}) => {
  const defaultTitle = AppConfig.site.title + ' - ' + AppConfig.site.tagline
  const pageTitle = AppConfig.site.title + ' - ' + AppConfig.site.tagline
  const canonicalURL = AppConfig.site.rootUri + location.pathname
  const LeftDrawerOpen = AppState.get('LeftDrawerOpen')
  const RightDrawerOpen = AppState.get('RightDrawerOpen')
  const initialRender = AppState.get('initialRender')
  const UserSettingsModalOpen = AppState.get('UserSettingsModalOpen')
  const drawerStates = classnames({LeftDrawerOpen, RightDrawerOpen})

  return {
    defaultTitle,
    pageTitle,
    canonicalURL,
    RightDrawerOpen,
    LeftDrawerOpen,
    initialRender,
    UserSettingsModalOpen,
    drawerStates
  }
}, App);
